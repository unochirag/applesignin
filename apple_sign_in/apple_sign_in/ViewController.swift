//
//  ViewController.swift
//  apple_sign_in
//
//  Created by Chirag on 24/03/20.
//  Copyright © 2020 Unoapp. All rights reserved.
//

import UIKit
import AuthenticationServices

class ViewController: AuthAppleIDController {

    @IBOutlet var btnAppleAuth: AuthAppleIDButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.completionHandler = { (flag, singInObject, err) in
            if(flag!){
                if let appleIDCredential = singInObject as? ASAuthorizationAppleIDCredential {
                    
                    let appleId = appleIDCredential.user
                    let appleUserFirstName = appleIDCredential.fullName?.givenName
                    let appleUserLastName = appleIDCredential.fullName?.familyName
                    let appleUserEmail = appleIDCredential.email
                    
                    print("appleId : \(appleId)")
                    print("apple FirstName : \(appleUserFirstName!)")
                    print("apple LastName : \(appleUserLastName!)")
                    print("apple Email : \(appleUserEmail!)")

                }
            }else{
                print(err!.localizedDescription)
                let alert = UIAlertController(title: "Error", message: err!.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func authorizationAppleIDButtonTapped(_ sender: AuthAppleIDButton) {
        self.handleLogInWithAppleIDButtonPress()
    }
    
}

