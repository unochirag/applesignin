//
//  AuthAppleIDButton.swift
//  test
//
//  Created by Chirag on 13/03/20.
//  Copyright © 2020 Unoapp. All rights reserved.
//

import UIKit
import AuthenticationServices

@IBDesignable
class AuthAppleIDButton: UIButton {

    private var appleAuthButton: ASAuthorizationAppleIDButton!
        
    @IBInspectable
    var cornerRadius: CGFloat = 6.0
    
    @IBInspectable
    var authButtonType: Int = ASAuthorizationAppleIDButton.ButtonType.default.rawValue
        
    @IBInspectable
    var authButtonStyle: Int = ASAuthorizationAppleIDButton.Style.white.rawValue
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        // Create ASAuthorizationAppleIDButton
        let type = ASAuthorizationAppleIDButton.ButtonType.init(rawValue: authButtonType) ?? .default
        let style = ASAuthorizationAppleIDButton.Style.init(rawValue: authButtonStyle) ?? .black
        appleAuthButton = ASAuthorizationAppleIDButton(authorizationButtonType: type,
                                                           authorizationButtonStyle: style)
        
        // Set selector for touch up inside event so that can forward the event to MyAuthorizationAppleIDButton
        appleAuthButton.addTarget(self, action: #selector(authorizationAppleIDButtonTapped(_:)), for: .touchUpInside)
        appleAuthButton.cornerRadius = cornerRadius
        // Show authorizationButton
        addSubview(appleAuthButton)

        // Use auto layout to make authorizationButton follow the MyAuthorizationAppleIDButton's dimension
        appleAuthButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            appleAuthButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 0.0),
            appleAuthButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0.0),
            appleAuthButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0.0),
            appleAuthButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0.0),
        ])
    }
    
    @objc func authorizationAppleIDButtonTapped(_ sender: Any) {
        // Forward the touch up inside event to MyAuthorizationAppleIDButton
        sendActions(for: .touchUpInside)
    }
}

class AuthAppleIDController: UIViewController, ASAuthorizationControllerDelegate {

    var completionHandler : ((_ success:Bool?,_ appleIDCredential:ASAuthorizationCredential?,_ error:Error?) -> Void)?
    
    func handleLogInWithAppleIDButtonPress() {
        if #available(iOS 13.0, *) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
        }
    }
     // ASAuthorizationControllerDelegate function for authorization failed

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
        self.completionHandler!(false,nil,error)
    }

    // ASAuthorizationControllerDelegate function for successful authorization

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        self.completionHandler!(true,authorization.credential,nil)
    }

}

extension AuthAppleIDController: ASAuthorizationControllerPresentationContextProviding {

    //For present window

    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }

}
